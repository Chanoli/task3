package AssignmentTask3;

public class PlayerCDominoCommand implements Playercommand{
	private PlayerCDomino doplacedomino;
	
	public PlayerCDominoCommand(PlayerCDomino placedomino) {
		this.doplacedomino = placedomino;
	}

	@Override
	public void execute() {
		doplacedomino.placeDomino();
	}
}
