package AssignmentTask3;

public class UndoplayerDominoCommand implements Playercommand{
	private PlayerCDomino undoplacedomino;
	
	public UndoplayerDominoCommand(PlayerCDomino placedomino) {
		this.undoplacedomino = placedomino;
	}

	@Override
	public void execute() {
		undoplacedomino.unplaceDomino();
	}

}
