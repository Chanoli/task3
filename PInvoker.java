package AssignmentTask3;

public class PInvoker {
	private Playercommand doCommand, undoCommand;
	public PInvoker(PlayerCDominoCommand doCommand) {
		this.doCommand = doCommand;
	}
	
	public PInvoker(PlayerCDominoCommand doCommand, UndoplayerDominoCommand undoCommand) {
		this.doCommand = doCommand;
		this.undoCommand = undoCommand;
	}
	
	public void setUndoInvoker(UndoplayerDominoCommand undoCommand) {
		this.undoCommand = undoCommand;
	}
	
	public void doPlace() {
		doCommand.execute();
	}
	
	public void undoPlace() {
		undoCommand.execute();
	}

}

